export class Product{
    articleId:string;// 4 digit max
    categories:string[];
    subCategories:string[];
    productTitle:string;
    productName:string;
    productDescription:string; //html format
    shippingPrice:number;
    sellingPrice:number;
    vendorId:string;// static
    minQuantity:number;
    maxQuantity:number;
    disclaimer:string; //html
    customerInputRequired:boolean=false; 
    customerInputMessage:string; 
    tags:string;
}

export class Category{
    id:string;
    name:string;
    displayName:string;
    displayOrder:number;
}

export class SubCategory{
    id:string;
    name:string;
    displayName:string;
    displayOrder:number;
    categoryId:string;
    category:Category;
}