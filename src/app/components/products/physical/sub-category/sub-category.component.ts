import { Component, OnInit } from '@angular/core';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {WebService} from '../../../../http/web-service.service';
import {Url} from '../../../../http/url-constants';
import {Category,SubCategory} from '../models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.scss']
})
export class SubCategoryComponent implements OnInit {
  public closeResult: string;
  public sub_categories = [];
  public subcategoryForm: FormGroup;
  public categories = [];
  public categoryObj=new SubCategory();

  constructor(private modalService: NgbModal,private fb: FormBuilder,private webService:WebService) {
    //this.sub_categories = categoryDB.category;
    this.subcategoryForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      dname: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      category:[''],
      order:['']
    });
  }

  open(content) {
    this.getAllCategory();
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getAllCategory() {
    this.webService.get(Url.API.category.get).subscribe((response) => {
      this.categories=response;
      if (response.status && response.status.code === 200) {
        //this.categories= 

      }
    }, () => {
      
    });
  }

  addSubCategory() {
    this.webService.post(Url.API.subcategory.add,this.categoryObj).subscribe((response) => {
      this.modalService.dismissAll();
      this.getAllSubCategory();
      if (response.status && response.status.code === 200) {
       this.modalService.dismissAll();
       this.categoryObj=new SubCategory();
       this.getAllSubCategory();
      }
    }, () => {
      
    });
  }

  getAllSubCategory(){
    this.webService.get(Url.API.subcategory.get).subscribe((response) => {
      this.sub_categories=response;
      if (response.status && response.status.code === 200) {
        //this.categories= 
      }
    }, () => {
      
    });
  }

  public settings = {
    actions: {
      position: 'right'
    },
    columns: {
      name: {
        title: 'Name',
      },
      displayName: {
        title: 'Display Name'
      },
      category: {
        title: 'Category',
        valuePrepareFunction: (data) => {
          return data.name;
        },
        filter:false
      },
      displayOrder: {
        title: 'Order',
        filter:false
      }
    },
  };

  ngOnInit() {
    this.getAllSubCategory();
  }

}
