import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {WebService} from '../../../../http/web-service.service';
import {Url} from '../../../../http/url-constants';
import {Category} from '../models';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  public categoryForm: FormGroup;
  public closeResult: string;
  public categories = []
  public categoryObj=new Category();
  constructor(private fb: FormBuilder,private modalService: NgbModal,private webService:WebService) {
    //this.categories = categoryDB.category;
    this.categoryForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      dname: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      order:['']
    })
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  public settings = {
    actions: {
      position: 'right'
    },
    columns: {    
      name: {
        title: 'Name'
      },
      displayName: {
        title: 'Display Name'
      },
      displayOrder: {
        title: 'Order',
        filter:false
      },
    },
  };

  ngOnInit() {
    this.getAllCategory();
  }

  getAllCategory(){
    this.webService.get(Url.API.category.get).subscribe((response) => {
      this.categories=response;
      if (response.status && response.status.code === 200) {
        //this.categories= 

      }
    }, () => {
      
    });
  }

  addCategory(){
    this.webService.post(Url.API.category.add,this.categoryObj).subscribe((response) => {
      this.modalService.dismissAll();
      this.getAllCategory();
      if (response.status && response.status.code === 200) {
       this.modalService.dismissAll();
       this.getAllCategory();
      }
    }, () => {
      
    });
  }

}
