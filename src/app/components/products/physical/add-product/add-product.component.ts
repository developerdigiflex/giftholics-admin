import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Product } from '../models';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { WebService } from '../../../../http/web-service.service';
import { Url } from '../../../../http/url-constants';
import { DomSanitizer } from '@angular/platform-browser';
import { ToasterService } from '../../../../core/toaster/toaster.service';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  public productForm: FormGroup;
  public counter = 1;
  public showSub = false;
  public productObj = new Product();
  public categories: any[] = [];
  public subCategories: any[] = [];
  public vendors: string[] = [];
  proImages: any[] = [];
  proImagesSrc: any[] = [];
  dProImages: any[] = [];
  dispImage: any;
  dDispImage: any;
  public customerInput = false;
  public imageChangedEvent: any = '';
  public croppedImage: any = '';
  public editorStyle = {
    height: '300px'
  };


  constructor(private fb: FormBuilder, private webService: WebService, private domSanitizer: DomSanitizer, private toasterService: ToasterService) {
    this.productForm = this.fb.group({
      title: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      article: ['', [Validators.required]],
      shipprice: ['', [Validators.required, Validators.pattern('^[+]?([0-9]+(?:[\\.][0-9]{0,2})?|\\.([0-9]+))$')]],
      sellprice: ['', [Validators.required, Validators.pattern('^[+]?([0-9]+(?:[\\.][0-9]{0,2})?|\\.([0-9]+))$')]],
      min: ['', [Validators.required, Validators.pattern(/^\d+$/)]],
      max: ['', [Validators.required, Validators.pattern(/^\d+$/)]],
      category: ['', [Validators.required]],
      subcategory: ['', [Validators.required]],
      //    code: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      //    size: ['', Validators.required],
      disclaimer: ['', Validators.required],
      inputR: [''],
      inputM: [''],
      desc: ['']
    });
  }

  getAllCategory() {
    this.webService.get(Url.API.category.get).subscribe((response) => {
      this.categories = response;
      if (response.status && response.status.code === 200) {
        //this.categories=

      }
    }, () => {

    });
  }

  getSubCategoryById() {
    this.webService.get(Url.API.subcategory.get).subscribe((response) => {
      this.subCategories = response;
      if (this.subCategories.length > 0) {
        this.showSub = true;
      } else {
        this.showSub = false;
      }
      if (response.status && response.status.code === 200) {
        //this.categories=
      }
    }, () => {

    });
  }

  imageLoaded() {
    /* show cropper */
  }

  cropperReady() {
    /* cropper ready */
  }

  loadImageFailed() {
    /* show message */
  }


  increment() {
    this.counter += 1;
  }

  decrement() {
    this.counter -= 1;
  }

  customerInputChange() {
    this.productObj.customerInputRequired = !this.productObj.customerInputRequired;
    if (!this.productObj.customerInputRequired) {
      this.productObj.customerInputMessage = '';
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.dispImage = this.base64ToFile(event.base64);
    this.dDispImage = event.base64;
  }

  base64ToFile(base64Image: string): Blob {
    const split = base64Image.split(',');
    const type = split[0].replace('data:', '').replace(';base64', '');
    const byteString = atob(split[1]);
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i += 1) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], {type});
  }

  //FileUpload
  readUrl(event: any, i) {
    if (event.target.files && event.target.files[0]) {

      if (!this.proImages[i]) {
        this.proImages[i] = event.target.files[0];
        this.proImagesSrc[i] = this.domSanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(event.target.files[0]));

        //this.dProImages[i] = URL.createObjectURL(event.target.files[0]);
      } else {
        this.proImages.push(event.target.files[0]);
        this.proImagesSrc.push(this.domSanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(event.target.files[0])));
        //this.dProImages.push(URL.createObjectURL(event.target.files[0]));
      }
    }


  }

  onChangeValue(e) {

  }

  chengeValue() {
    this.getSubCategoryById();

  }

  ngOnInit() {
    this.getAllCategory();
    this.dDispImage = 'assets/images/user.png';
    this.dProImages = ['assets/images/user.png', 'assets/images/user.png', 'assets/images/user.png', 'assets/images/user.png', 'assets/images/user.png'];
    this.vendors = ['vendor 1', 'vendor 2', 'vendor 3'];
  }

  addProduct() {
    const formData = new FormData();
    for (let i = 0; i < this.proImages.length; i++) {
      formData.append('file', this.proImages[i]);
    }
    formData.append('file1', this.dispImage);
    formData.append('data', JSON.stringify(this.productObj));
    this.webService.multipartFileUpload(Url.API.product.create, formData)
      .subscribe((response) => {
        this.toasterService.show('Successfully Added', {classname: 'bg-success text-light', delay: 10000});
        console.log(response);
      }, (reject) => {

      });
  }

  show() {
    console.log(this.productForm);
  }
}
