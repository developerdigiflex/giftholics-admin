/**
 * Created by mayur.
 */

export class Url {
  public static serverUrl = 'http://ec2-13-235-13-125.ap-south-1.compute.amazonaws.com:8582/api';
  public static API = {
    product: {
      create: '/product/add'
    },
    category: {
      add: '/category/add',
      get: '/category/getAll'
    },
    subcategory: {
      add: '/sub-category/add',
      get: '/sub-category/getAll',
      getbyId: '/sub-category/getByCategory/',

    }

  };
}
