import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToasterContainerComponent } from './toaster/toaster-container.component';
import { NgbToastModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [ToasterContainerComponent],
  exports: [
    ToasterContainerComponent
  ],
  imports: [
    CommonModule,
    NgbToastModule
  ]
})
export class CoreModule {
}
